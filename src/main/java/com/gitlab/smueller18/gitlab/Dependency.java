package com.gitlab.smueller18.gitlab;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@CompoundIndexes({
        @CompoundIndex(name = "dependency", def = "{'groupId' : 1, 'artifactId': 1, 'version': 1}", unique = true)
})
public class Dependency {
    @Id
    String id;
    String groupId;
    String artifactId;
    String version;

    Dependency(String groupId, String artifactId, String version) {
        this.groupId = groupId;
        this.artifactId = artifactId;
        this.version = version;
        this.id = this.groupId + ":" + this.artifactId + ":" + this.version;
    }

    @Override
    public String toString() {
        return String.format(
                "Dependency[groupId='%s', artifactId='%s', version='%s']",
                groupId, artifactId, version);
    }

}
