package com.gitlab.smueller18.gitlab;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@CompoundIndexes({
        @CompoundIndex(name = "reference", def = "{'projectId' : 1, 'branch': 1, 'dependency': 1}", unique = true)
})
public class DownstreamReference {

    @Id
    private String id;
    String projectId;
    String projectPath;
    String branch;
    @DBRef
    public Dependency dependency;

    DownstreamReference(String projectId, String projectPath, String branch, Dependency dependency) {
        this.projectId = projectId;
        this.projectPath = projectPath;
        this.branch = branch;
        this.dependency = dependency;
        this.id = this.projectId + ":" + this.branch + ":" + this.dependency.id;
    }

    @Override
    public String toString() {
        return String.format(
                "DownstreamReference[projectId=%s, projectPath='%s', branch='%s', dependency='%s']",
                projectId, projectPath, branch, dependency
        );
    }
}
