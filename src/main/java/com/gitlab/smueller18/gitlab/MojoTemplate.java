package com.gitlab.smueller18.gitlab;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.client.MongoClients;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.security.InvalidParameterException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class MojoTemplate extends AbstractMojo {

    static final String MONGO_IMAGE = "mongo:4.2.8";

    @Parameter(defaultValue = "${project}", readonly = true, required = true)
    MavenProject project;

    @Parameter(defaultValue = "${env.CI_COMMIT_REF_NAME}", property = "ci.commit.ref.name", required = true)
    String ciCommitRefName;

    @Parameter(defaultValue = "${env.CI_COMMIT_TAG}", property = "ci.commit.tag")
    String ciCommitTag;

    @Parameter(defaultValue = "${env.CI_COMMIT_SHA}", property = "ci.commit.sha")
    String ciCommitSha;

    @Parameter(defaultValue = "${env.CI_PROJECT_ID}", property = "ci.project.id")
    String ciProjectId;

    @Parameter(defaultValue = "${env.CI_PROJECT_URL}", property = "ci.project.url")
    String ciProjectUrl;

    @Parameter(defaultValue = "${env.CI_SERVER_HOST}", property = "ci.server.host", required = true)
    String ciServerHost;

    @Parameter(defaultValue = "${env.CI_JOB_TOKEN}", property = "ci.job.token", required = true)
    String ciJobToken;

    @Parameter(defaultValue = "${env.MONGO_HOST}", property = "mongo.host")
    String mongoHost;

    @Parameter(defaultValue = "${env.MONGO_PORT}", property = "mongo.port")
    int mongoPort;

    @Parameter(defaultValue = "${env.MONGO_DATABASE}", property = "mongo.database.name")
    String mongoDatabaseName;

    @Parameter(defaultValue = "${env.MONGO_USERNAME}", property = "mongo.database.username")
    String mongoDatabaseUsername;

    @Parameter(defaultValue = "${env.MONGO_PASSWORD}", property = "mongo.database.password")
    String mongoDatabasePassword;

    @Parameter(property = "group.id.regex")
    String groupIdRegex;

    Pattern groupIdPattern;

    Dependency dependency;

    MongoTemplate mongoTemplate;

    @Override
    public void execute() throws MojoExecutionException {
        throw new NotImplementedException("not implemented");
    }

    void initialize() {
        if (mongoHost == null) {
            mongoHost = "localhost";
        }
        if (mongoPort == 0) {
            mongoPort = 27017;
        }
        if (mongoDatabaseName == null) {
            mongoDatabaseName = "gitlab-dependency";
        }
        if (groupIdRegex != null) {
            this.groupIdPattern = Pattern.compile(groupIdRegex);
        }

        if (mongoDatabaseUsername == null) {
            throw new InvalidParameterException("Username must be set");
        }
        if (mongoDatabasePassword == null) {
            throw new InvalidParameterException("Password must be set");
        }

        if (mongoTemplate == null) {
            mongoTemplate = mongoTemplate(mongoHost, mongoPort, mongoDatabaseName, mongoDatabaseUsername, mongoDatabasePassword);
        }

        dependency = new Dependency(project.getGroupId(), project.getArtifactId(), project.getVersion());
    }

    public static MongoTemplate mongoTemplate(String host, int port, String databaseName, String username, String password) {
        MongoClientSettings clientSettings = MongoClientSettings
                .builder()
                .retryReads(true).retryWrites(true)
                .applyConnectionString(new ConnectionString(String.format("mongodb://%s:%s", host, port)))
                .credential(MongoCredential.createCredential(username, databaseName, password.toCharArray()))
                .build();
        return new MongoTemplate(MongoClients.create(clientSettings), databaseName);
    }

}
