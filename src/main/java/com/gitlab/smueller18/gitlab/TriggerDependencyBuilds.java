package com.gitlab.smueller18.gitlab;

import org.apache.http.HttpStatus;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;

import java.util.Collections;

/**
 * Goal which triggers downstream jobs in GitLab
 */
@Mojo(name = "trigger", defaultPhase = LifecyclePhase.DEPLOY)
public class TriggerDependencyBuilds extends MojoTemplate {

    private static final Logger logger = LogManager.getLogger(TriggerDependencyBuilds.class);

    GitLabApi gitLabApi;

    protected void initialize() {
        super.initialize();
        if (gitLabApi == null) {
            gitLabApi = new GitLabApi("https://" + ciServerHost, ciJobToken);
            gitLabApi.enableRequestResponseLogging();
        }
    }

    public void execute() throws MojoExecutionException {

        this.initialize();

        try {
            mongoTemplate.listDownstreamReferences(dependency).forEach(downstreamReference -> {
                try {
                    gitLabApi.getPipelineApi().triggerPipeline(downstreamReference.projectId, ciJobToken, downstreamReference.branch, Collections.emptyList());
                    logger.info(String.format("Triggered branch %s of project %s (id: %s)",
                            downstreamReference.branch, downstreamReference.projectPath, downstreamReference.projectId));
                } catch (GitLabApiException e) {
                    if (e.getHttpStatus() == HttpStatus.SC_BAD_REQUEST && e.getMessage().equals("The following fields have validation errors: base")) {
                        logger.info(String.format("Branch %s of project %s (id: %s) does not exists anymore.",
                                downstreamReference.branch, downstreamReference.projectPath, downstreamReference.projectId));
                        mongoTemplate.deleteDownstreamReference(downstreamReference);
                    } else if(e.getHttpStatus() == HttpStatus.SC_NOT_FOUND) {
                        logger.info(String.format("Project %s (id: %s) does not exists anymore.",
                                downstreamReference.projectPath, downstreamReference.projectId));
                        mongoTemplate.deleteDownstreamReference(downstreamReference);
                    }
                    else {
                        throw new RuntimeException(String.format("Error while triggering jobs (HttpStatusCode: %s)", e.getHttpStatus()), e);
                    }
                }
            });
        } catch (Exception e) {
            throw new RuntimeException("Failed triggering downstream jobs", e);
        }

    }
}
