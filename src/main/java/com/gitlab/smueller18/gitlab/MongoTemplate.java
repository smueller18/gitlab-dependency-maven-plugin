package com.gitlab.smueller18.gitlab;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.result.DeleteResult;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;

import java.util.ArrayList;
import java.util.List;

public class MongoTemplate extends org.springframework.data.mongodb.core.MongoTemplate {

    private static final Logger logger = LogManager.getLogger(MongoTemplate.class);

    public MongoTemplate(MongoClient mongoClient, String databaseName) {
        super(mongoClient, databaseName);
    }

    /**
     * This is a very ugly hack that this method can be mocked with Mockito
     * @param object
     * @return
     */
    DeleteResult removeObject(Object object) {
        return super.remove(object);
    }

    @Override
    public DeleteResult remove(Object object) {
        throw new NotImplementedException();
    }

    /**
     * This is a very ugly hack that this method can be mocked with Mockito
     */
    public <T> T insertObject(T objectToSave) {
        return super.insert(objectToSave);
    }

    @Override
    public <T> T insert(T objectToSave) {
        throw new NotImplementedException();
    }

    public Dependency getDependency(Dependency dependency) {
        Query query = new Query();
        query.addCriteria(Criteria.where("groupId").is(dependency.groupId));
        query.addCriteria(Criteria.where("artifactId").is(dependency.artifactId));
        query.addCriteria(Criteria.where("version").is(dependency.version));
        try {
            return findOne(query, Dependency.class);
        } catch (Exception e) {
            throw new RuntimeException("Failed getting dependency from mongodb", e);
        }
    }

    /**
     * Inserts dependency if not exists and returns corresponding dependency or just returns dependency otherwise.
     *
     * @param dependency Maven dependency information
     * @return Mongodb dependency object
     */
    public Dependency insertAndGetDependency(Dependency dependency) {
        Dependency mongoDependency = getDependency(dependency);
        if (mongoDependency == null) {
            try {
                mongoDependency = insertObject(dependency);
            } catch (Exception e) {
                throw new RuntimeException(String.format("Failed inserting %s", dependency), e);
            }
        }
        return mongoDependency;
    }

    public List<DownstreamReference> listDownstreamReferences(String projectId, String branch) {
        Query query = new Query();
        query.addCriteria(Criteria.where("projectId").is(projectId));
        query.addCriteria(Criteria.where("branch").is(branch));
        try {
            return find(query, DownstreamReference.class);
        } catch (Exception e) {
            throw new RuntimeException("Failed listing downstream references", e);
        }
    }

    public List<DownstreamReference> listDownstreamReferences(Dependency dependency) {
        Query query = new Query();
        query.addCriteria(Criteria.where("dependency.$id").is(dependency.id));
        try {
            return find(query, DownstreamReference.class);
        } catch (Exception e) {
            throw new RuntimeException("Failed listing downstream references", e);
        }
    }

    public List<DeleteResult> deleteAllDownstreamReferences(String projectId, String branch) {
        List<DeleteResult> deleteResults = new ArrayList<>();
        listDownstreamReferences(projectId, branch).forEach(downstreamReference -> {
            try {
                logger.info(String.format("Removing %s", downstreamReference));
                deleteResults.add(removeObject(downstreamReference));
            } catch (Exception e) {
                throw new RuntimeException(String.format("Failed deleting %s", downstreamReference), e);
            }
        });
        return deleteResults;
    }

    public DeleteResult deleteDownstreamReference(DownstreamReference downstreamReference) {
        try {
            logger.info(String.format("Removing %s", downstreamReference));
            return removeObject(downstreamReference);
        } catch (Exception e) {
            throw new RuntimeException(String.format("Failed deleting %s", downstreamReference), e);
        }
    }

    public DownstreamReference insertDownstreamReference(DownstreamReference downstreamReference) {
        try {
            insertAndGetDependency(downstreamReference.dependency);
            logger.info(String.format("Inserting downstreamReference %s", downstreamReference));
            return insertObject(downstreamReference);
        } catch (Exception e) {
            throw new RuntimeException("Failed inserting downstream reference", e);
        }
    }
}
