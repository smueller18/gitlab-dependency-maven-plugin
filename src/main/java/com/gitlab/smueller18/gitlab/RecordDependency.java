package com.gitlab.smueller18.gitlab;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Goal which records dependencies
 */
@Mojo(name = "record", defaultPhase = LifecyclePhase.DEPLOY)
public class RecordDependency extends MojoTemplate {

    private static final Logger logger = LogManager.getLogger(RecordDependency.class);

    public void execute() throws MojoExecutionException {

        if (ciCommitTag != null) {
            return;
        }

        this.initialize();

        mongoTemplate.deleteAllDownstreamReferences(ciProjectId, ciCommitRefName);

        List<Dependency> dependencies = new ArrayList<>();

        if (project.getParent() != null) {
            dependencies.add(new Dependency(project.getParent().getGroupId(), project.getParent().getArtifactId(), project.getParent().getVersion()));
        }
        project.getDependencies().forEach(mavenDependency ->
                dependencies.add(new Dependency(mavenDependency.getGroupId(), mavenDependency.getArtifactId(), mavenDependency.getVersion()))
        );

        dependencies.forEach(dependency -> {
            if (groupIdPattern != null && !groupIdPattern.matcher(dependency.groupId).matches()) {
                logger.debug(String.format("GroupId %s does not matches regex '%s'", dependency.groupId, groupIdRegex));
                return;
            }
            logger.debug(String.format("GroupId %s matches regex '%s'", dependency.groupId, groupIdRegex));

            if (!dependency.version.endsWith("-SNAPSHOT")) {
                return;
            }

            logger.debug(String.format("Found snapshot dependency %s", dependency));

            mongoTemplate.insertDownstreamReference(
                    new DownstreamReference(ciProjectId, ciProjectUrl, ciCommitRefName, new Dependency(
                            dependency.groupId,
                            dependency.artifactId,
                            dependency.version
                    ))
            );
        });
    }
}
