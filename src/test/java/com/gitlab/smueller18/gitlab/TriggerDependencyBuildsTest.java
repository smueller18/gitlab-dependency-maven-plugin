package com.gitlab.smueller18.gitlab;

import org.apache.maven.plugin.testing.MojoRule;
import org.apache.maven.plugin.testing.WithoutMojo;
import org.apache.maven.project.MavenProject;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Pipeline;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class TriggerDependencyBuildsTest {

    @Rule
    public MojoRule rule = new MojoRule() {
        @Override
        protected void before() throws Throwable {
        }

        @Override
        protected void after() {
        }
    };

    @Test
    @WithoutMojo
    public void initialize() {
        TriggerDependencyBuilds triggerDependencyBuilds = new TriggerDependencyBuilds();
        triggerDependencyBuilds.project = new MavenProject();
        triggerDependencyBuilds.project.setGroupId("");
        triggerDependencyBuilds.project.setArtifactId("");
        triggerDependencyBuilds.project.setVersion("");
        triggerDependencyBuilds.mongoDatabaseUsername = "";
        triggerDependencyBuilds.mongoDatabasePassword = "";
        triggerDependencyBuilds.initialize();
    }

    @Test
    public void execute() throws Exception {

        File pom = new File("src/test/resources/project-to-test/");
        Assert.assertNotNull(pom);
        assertTrue(pom.exists());

        TriggerDependencyBuilds triggerDependencyBuilds = (TriggerDependencyBuilds) rule.lookupConfiguredMojo(pom, "trigger");
        Assert.assertNotNull(triggerDependencyBuilds);
        triggerDependencyBuilds.gitLabApi = gitLabApiMock();
        triggerDependencyBuilds.mongoTemplate = mongoTemplateMock();
        triggerDependencyBuilds.execute();
    }

    @Test
    public void executeWithExceptions() throws Exception {
        TriggerDependencyBuilds triggerDependencyBuilds = new TriggerDependencyBuilds();
        triggerDependencyBuilds.project = new MavenProject();
        triggerDependencyBuilds.project.setGroupId("");
        triggerDependencyBuilds.project.setArtifactId("");
        triggerDependencyBuilds.project.setVersion("");
        triggerDependencyBuilds.ciJobToken = "";
        triggerDependencyBuilds.mongoDatabaseUsername = "";
        triggerDependencyBuilds.mongoDatabasePassword = "";
        triggerDependencyBuilds.gitLabApi = gitLabApiMock();
        triggerDependencyBuilds.mongoTemplate = mongoTemplateMock();

        RuntimeException e = Assertions.assertThrows(RuntimeException.class, triggerDependencyBuilds::execute);
        assert e.getMessage().equals("Failed triggering downstream jobs");

        triggerDependencyBuilds.project.setGroupId("test");
        triggerDependencyBuilds.dependency = new Dependency("", "", "");
        e = Assertions.assertThrows(RuntimeException.class, triggerDependencyBuilds::execute);
        assert e.getMessage().equals("Failed triggering downstream jobs");
        assert e.getCause().getMessage().equals("Error while triggering jobs (HttpStatusCode: 400)");

        triggerDependencyBuilds.project.setGroupId("not-existent");
        Assertions.assertDoesNotThrow(triggerDependencyBuilds::execute);
    }

    private GitLabApi gitLabApiMock() throws Exception {
        GitLabApi gitLabApiMock = Mockito.mock(GitLabApi.class, Mockito.RETURNS_DEEP_STUBS);
        when(gitLabApiMock.getPipelineApi().triggerPipeline("2", "", "develop", Collections.emptyList()))
                .then(invocationOnMock -> new Pipeline());
        when(gitLabApiMock.getPipelineApi().triggerPipeline("0", "", "develop", Collections.emptyList()))
                .then(invocationOnMock -> {
                    throw new GitLabApiException("", 400);
                });
        when(gitLabApiMock.getPipelineApi().triggerPipeline("3", "", "develop", Collections.emptyList()))
                .then(invocationOnMock -> {
                    throw new GitLabApiException("The following fields have validation errors: base", 400);
                });
        when(gitLabApiMock.getPipelineApi().triggerPipeline("4", "", "develop", Collections.emptyList()))
                .then(invocationOnMock -> {
                    throw new GitLabApiException("", 404);
                });
        return gitLabApiMock;
    }

    @Test
    @WithoutMojo
    public void testGitlab() throws Exception {
        assert gitLabApiMock().getPipelineApi().triggerPipeline("2", "", "develop", Collections.emptyList()) != null;
    }

    private MongoTemplate mongoTemplateMock() {
        MongoTemplate mongoConfigMock = Mockito.mock(MongoTemplate.class, Mockito.RETURNS_DEEP_STUBS);

        List<DownstreamReference> downstreamReferences = new ArrayList<>() {{
            add(new DownstreamReference("1", "1", "develop",
                    new Dependency("com.gitlab.smueller18.gitlab", "maven-test-dependency", "1.0-SNAPSHOT")));
        }};

        when(mongoConfigMock.listDownstreamReferences(any(), any())).then(invocationOnMock -> {
            if (invocationOnMock.getArguments()[0] == "1" && invocationOnMock.getArguments()[1] == "develop") {
                return downstreamReferences;
            }
            throw new RuntimeException("Failed listing downstream references");
        });

        when(mongoConfigMock.listDownstreamReferences(any())).then(invocationOnMock -> {
            switch (invocationOnMock.getArguments()[0].toString()) {
                case "Dependency[groupId='test', artifactId='', version='']":
                    return new ArrayList<>() {{
                        add(new DownstreamReference("0", "0", "develop", new Dependency("", "", "")));
                    }};
                case "Dependency[groupId='not-existent', artifactId='', version='']":
                    return new ArrayList<>() {{
                        add(new DownstreamReference("3", "0", "develop", new Dependency("", "", "")));
                    }};
                case "Dependency[groupId='com.gitlab.smueller18.gitlab', artifactId='maven-test-dependency', version='1.0-SNAPSHOT']":
                    return downstreamReferences;
            }
            throw new RuntimeException("Failed listing downstream references");
        });

        return mongoConfigMock;
    }

    @Test
    @WithoutMojo
    public void mongoMockTest() {
        MongoTemplate mongoTemplateMock = mongoTemplateMock();
        assert mongoTemplateMock.listDownstreamReferences("1", "develop").size() == 1;

        Assertions.assertThrows(RuntimeException.class, () ->
                mongoTemplateMock.listDownstreamReferences("2", "develop")
        );

        Assertions.assertThrows(RuntimeException.class, () ->
                mongoTemplateMock.listDownstreamReferences("4", "develop")
        );

        assert mongoTemplateMock.listDownstreamReferences(
                new Dependency("com.gitlab.smueller18.gitlab", "maven-test-dependency", "1.0-SNAPSHOT")
        ).size() == 1;

        Assertions.assertThrows(RuntimeException.class, () ->
                mongoTemplateMock.listDownstreamReferences(new Dependency("", "", "")));
    }

}
