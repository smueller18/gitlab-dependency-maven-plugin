package com.gitlab.smueller18.gitlab;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.client.MongoClients;
import com.mongodb.client.result.DeleteResult;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.utility.DockerImageName;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class MongoTemplateTest {

    private final static String mongoDatabaseName = "admin";
    private final static String mongoDatabaseUsername = "admin";
    private final static String mongoDatabasePassword = "admin";

    private MongoTemplate mongoTemplate;
    private MongoTemplate invalidMongoTemplate;

    @ClassRule
    public static GenericContainer mongo = new GenericContainer<>(DockerImageName.parse(MojoTemplate.MONGO_IMAGE))
            .withExposedPorts(27017)
            .withEnv("MONGO_INITDB_DATABASE", mongoDatabaseName)
            .withEnv("MONGO_INITDB_ROOT_USERNAME", mongoDatabaseUsername)
            .withEnv("MONGO_INITDB_ROOT_PASSWORD", mongoDatabasePassword);

    @Before
    public void setUp() {
        String mongoAddress = mongo.getContainerIpAddress();
        Integer mongoPort = mongo.getFirstMappedPort();
        mongoTemplate = MojoTemplate.mongoTemplate(mongoAddress, mongoPort, mongoDatabaseName, mongoDatabaseUsername, mongoDatabasePassword);

        MongoClientSettings clientSettings = MongoClientSettings
                .builder()
                .retryReads(true).retryWrites(true)
                .applyConnectionString(new ConnectionString(String.format("mongodb://%s:%s", mongoAddress + "-invalid", mongoPort)))
                .applyToClusterSettings(builder -> builder
                        .serverSelectionTimeout(0, TimeUnit.SECONDS)
                )
                .applyToSocketSettings(builder -> builder
                        .connectTimeout(0, TimeUnit.SECONDS)
                        .readTimeout(0, TimeUnit.SECONDS))
                .credential(MongoCredential.createCredential(mongoDatabaseUsername, mongoDatabaseName, mongoDatabasePassword.toCharArray()))
                .build();
        invalidMongoTemplate = new MongoTemplate(MongoClients.create(clientSettings), mongoDatabaseName);
    }

    @Test
    public void getDependency() {
        assert mongoTemplate.getDependency(new Dependency("test", "test", "1.0-SNAPSHOT")) == null;

        RuntimeException e = Assertions.assertThrows(RuntimeException.class, () ->
                invalidMongoTemplate.getDependency(new Dependency("", "", "")));
        assert e.getMessage().equals("Failed getting dependency from mongodb");
    }

    @Test
    public void insertAndGetDependency() throws Exception {
        Dependency mongoDependency = mongoTemplate.insertAndGetDependency(new Dependency("test", "test", "1.0"));
        assert mongoDependency.toString().equals("Dependency[groupId='test', artifactId='test', version='1.0']");

        RuntimeException e = Assertions.assertThrows(RuntimeException.class, () ->
                invalidMongoTemplate.getDependency(new Dependency("", "", "")));
        assert e.getMessage().equals("Failed getting dependency from mongodb");

        MongoTemplate mongoTemplateMock = Mockito.mock(MongoTemplate.class, Mockito.RETURNS_DEEP_STUBS);
        when(mongoTemplateMock.getDependency(any())).thenReturn(null);
        when(mongoTemplateMock.insertAndGetDependency(any())).thenCallRealMethod();
        when(mongoTemplateMock.insertObject(any())).thenThrow(new RuntimeException());
        e = Assertions.assertThrows(RuntimeException.class, () ->
                mongoTemplateMock.insertAndGetDependency(new Dependency("test", "test", "1.0")));
        assert e.getMessage().equals("Failed inserting Dependency[groupId='test', artifactId='test', version='1.0']");
    }

    @Test
    public void listDownstreamDependencies() {
        List<DownstreamReference> downstreamReferences = mongoTemplate.listDownstreamReferences("0", "develop");
        assert downstreamReferences.size() == 0;

        RuntimeException e = Assertions.assertThrows(RuntimeException.class, () ->
                invalidMongoTemplate.listDownstreamReferences("", ""));
        assert e.getMessage().equals("Failed listing downstream references");

        downstreamReferences = mongoTemplate.listDownstreamReferences(new Dependency("test", "test", "1.1"));
        assert downstreamReferences.size() == 0;

        e = Assertions.assertThrows(RuntimeException.class, () ->
                invalidMongoTemplate.listDownstreamReferences(new Dependency("", "", "")));
        assert e.getMessage().equals("Failed listing downstream references");

        Dependency dependency = new Dependency("test", "test", "1.2");
        mongoTemplate.insertDownstreamReference(new DownstreamReference("1", "1", "develop", dependency));

        downstreamReferences = mongoTemplate.listDownstreamReferences("1", "develop");
        assert downstreamReferences.size() == 1;
        assert downstreamReferences.get(0).toString()
                .equals("DownstreamReference[projectId=1, projectPath='1', branch='develop'," +
                        " dependency='Dependency[groupId='test', artifactId='test', version='1.2']']");

        downstreamReferences = mongoTemplate.listDownstreamReferences(dependency);
        assert downstreamReferences.size() == 1;
        assert downstreamReferences.get(0).toString()
                .equals("DownstreamReference[projectId=1, projectPath='1', branch='develop'," +
                        " dependency='Dependency[groupId='test', artifactId='test', version='1.2']']");
    }

    @Test
    public void deleteAllDownstreamReferences() throws Exception {
        Dependency dependency = new Dependency("test", "test", "1.3");
        mongoTemplate.insertDownstreamReference(new DownstreamReference("2", "2", "develop", dependency));
        List<DeleteResult> deleteResults = mongoTemplate.deleteAllDownstreamReferences("2", "develop");
        assert deleteResults.size() == 1;
        assert deleteResults.get(0).getDeletedCount() == 1;

        RuntimeException e = Assertions.assertThrows(RuntimeException.class, () ->
                invalidMongoTemplate.deleteAllDownstreamReferences("", ""));
        assert e.getMessage().equals("Failed listing downstream references");

        MongoTemplate mongoTemplateMock = Mockito.mock(MongoTemplate.class, Mockito.RETURNS_DEEP_STUBS);
        when(mongoTemplateMock.listDownstreamReferences(any(), any())).thenReturn(List.of(new DownstreamReference("4", "4", "develop", dependency)));
        when(mongoTemplateMock.deleteAllDownstreamReferences(any(), any())).thenCallRealMethod();
        when(mongoTemplateMock.removeObject(any())).thenThrow(new RuntimeException());
        e = Assertions.assertThrows(RuntimeException.class, () ->
                mongoTemplateMock.deleteAllDownstreamReferences("4", "develop"));
        assert e.getMessage().equals("Failed deleting DownstreamReference[projectId=4, projectPath='4', branch='develop'," +
                " dependency='Dependency[groupId='test', artifactId='test', version='1.3']']");
    }

    @Test
    public void deleteDownstreamReference() {
        Dependency dependency = new Dependency("test", "test", "1.5");
        DownstreamReference downstreamReference = mongoTemplate.insertDownstreamReference(
                new DownstreamReference("2", "2", "develop", dependency));

        DeleteResult deleteResult = mongoTemplate.deleteDownstreamReference(downstreamReference);
        assert deleteResult.getDeletedCount() == 1;

        RuntimeException e = Assertions.assertThrows(RuntimeException.class, () ->
                invalidMongoTemplate.deleteDownstreamReference(new DownstreamReference("", "", "", dependency)));
        assert e.getMessage().equals("Failed deleting DownstreamReference[projectId=, projectPath='', branch=''," +
                " dependency='Dependency[groupId='test', artifactId='test', version='1.5']']");
    }

    @Test
    public void insertDownstreamReference() {
        Dependency dependency = new Dependency("test", "test", "1.4");
        mongoTemplate.insertDownstreamReference(new DownstreamReference("3", "3", "develop", dependency));

        RuntimeException e = Assertions.assertThrows(RuntimeException.class, () ->
                invalidMongoTemplate.insertDownstreamReference(new DownstreamReference("3", "3", "develop", dependency)));
        assert e.getMessage().equals("Failed inserting downstream reference");
    }

}
