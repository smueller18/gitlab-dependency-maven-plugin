package com.gitlab.smueller18.gitlab;

import org.apache.commons.lang3.NotImplementedException;
import org.apache.maven.project.MavenProject;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.security.InvalidParameterException;

public class MojoTemplateTest {
    @Test
    public void MojoTemplate() {
        Assertions.assertThrows(NotImplementedException.class, () -> {
            MojoTemplate mojoTemplate = new MojoTemplate();
            mojoTemplate.execute();
        });
    }

    @Test
    public void initialize() {
        MojoTemplate mojoTemplate = new MojoTemplate();
        mojoTemplate.project = new MavenProject();
        mojoTemplate.project.setGroupId("");
        mojoTemplate.project.setArtifactId("");
        mojoTemplate.project.setVersion("");
        mojoTemplate.mongoDatabaseUsername = "";
        mojoTemplate.mongoDatabasePassword = "";
        mojoTemplate.initialize();
    }

    @Test
    public void initializeMissingUsername() {
        MojoTemplate mojoTemplate = new MojoTemplate();
        Exception e = Assertions.assertThrows(InvalidParameterException.class, mojoTemplate::initialize);
        assert e.getMessage().equals("Username must be set");
    }

    @Test
    public void initializeMissingPassword() {
        MojoTemplate mojoTemplate = new MojoTemplate();
        mojoTemplate.mongoDatabaseUsername = "";
        Exception e = Assertions.assertThrows(InvalidParameterException.class, mojoTemplate::initialize);
        assert e.getMessage().equals("Password must be set");
    }
}
