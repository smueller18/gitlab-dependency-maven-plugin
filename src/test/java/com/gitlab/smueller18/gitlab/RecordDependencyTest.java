package com.gitlab.smueller18.gitlab;


import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.testing.MojoRule;
import org.apache.maven.plugin.testing.WithoutMojo;
import org.junit.*;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.utility.DockerImageName;

import java.io.File;

import static org.junit.Assert.assertTrue;

public class RecordDependencyTest {

    private final static String mongoDatabaseName = "admin";
    private final static String mongoDatabaseUsername = "admin";
    private final static String mongoDatabasePassword = "admin";

    private String mongoAddress;
    private Integer mongoPort;

    @ClassRule
    public static GenericContainer mongo = new GenericContainer<>(DockerImageName.parse(MojoTemplate.MONGO_IMAGE))
            .withExposedPorts(27017)
            .withEnv("MONGO_INITDB_DATABASE", mongoDatabaseName)
            .withEnv("MONGO_INITDB_ROOT_USERNAME", mongoDatabaseUsername)
            .withEnv("MONGO_INITDB_ROOT_PASSWORD", mongoDatabasePassword);

    @Before
    public void setUp() {
        mongoAddress = mongo.getContainerIpAddress();
        mongoPort = mongo.getFirstMappedPort();
    }

    @Rule
    public MojoRule rule = new MojoRule() {
        @Override
        protected void before() throws Throwable {
        }

        @Override
        protected void after() {
        }
    };

    public void execute(String pomFile) throws Exception {

        File pom = new File(pomFile);
        Assert.assertNotNull(pom);
        assertTrue(pom.exists());

        RecordDependency recordDependency = (RecordDependency) rule.lookupConfiguredMojo(pom, "record");
        Assert.assertNotNull(recordDependency);
        recordDependency.mongoTemplate = MojoTemplate.mongoTemplate(
                mongoAddress, mongoPort, recordDependency.mongoDatabaseUsername, recordDependency.mongoDatabaseUsername, recordDependency.mongoDatabasePassword
        );
        recordDependency.execute();
    }

    @Test
    public void executeProject() throws Exception {
        execute("src/test/resources/project-to-test/");
    }

    @Test
    public void executeProject2() throws Exception {
        execute("src/test/resources/project-to-test2/");
    }

    @Test
    @WithoutMojo
    public void executeWithCiCommitTag() throws MojoExecutionException {
        RecordDependency recordDependency = new RecordDependency();
        recordDependency.ciCommitTag = "1.0";
        recordDependency.execute();
    }

}
