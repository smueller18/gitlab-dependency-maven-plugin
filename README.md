# gitlab-dependency-maven-plugin

[![pipeline status](https://gitlab.com/smueller18/gitlab-dependency-maven-plugin/badges/develop/pipeline.svg)](https://gitlab.com/smueller18/gitlab-dependency-maven-plugin/commits/master)
[![coverage report](https://gitlab.com/smueller18/gitlab-dependency-maven-plugin/badges/develop/coverage.svg)](https://gitlab.com/smueller18/gitlab-dependency-maven-plugin/commits/master)

Plugin for downstream maven snapshot dependency build triggers for GitLab

## Requirements

-   Java >= 11
-   Maven >= 3.6.0
-   GitLab >= 12.0 Premium/Silver ([Multi-project pipelines](https://docs.gitlab.com/ee/ci/multi_project_pipelines.html))
-   External MongoDB

**Special requirements for development**

-   Docker

## Restrictions

This plugin currently does not support:

-   Multi-module projects

## Usage

This plugin needs an external MongoDB to persist dependency tree information. Have a look at the
[official Dockerhub image](https://hub.docker.com/_/mongo) how to run a MongoDB image pretty simple.

Ensure a gitlab-maven server entry in the `settings.xml`, which must be provided by the CI, to allow triggering jobs through GitLab API:

```xml
<settings xmlns="http://maven.apache.org/SETTINGS/1.1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd">
  <servers>
    <server>
      <id>gitlab-maven</id>
      <configuration>
        <httpHeaders>
          <property>
            <name>Job-Token</name>
            <value>${env.CI_JOB_TOKEN}</value>
          </property>
        </httpHeaders>
      </configuration>
    </server>
  </servers>
</settings>
```

Add the plugin to your project pom and adjust configuration for your needs. Have a look at
[MojoTemplate.java](https://gitlab.com/smueller18/gitlab-dependency-maven-plugin/blob/master/src/main/java/com/gitlab/smueller18/gitlab/MojoTemplate.java)
to see what parameters and environment variables can be set.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>com.gitlab.smueller18</groupId>
  <artifactId>maven-test</artifactId>
  <version>1.0-SNAPSHOT</version>
  <packaging>jar</packaging>
  <name>Maven Test Project</name>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <maven.compiler.source>11</maven.compiler.source>
    <maven.compiler.target>11</maven.compiler.target>
  </properties>

  <dependencies>
    <dependency>
      <groupId>com.gitlab.smueller18</groupId>
      <artifactId>maven-test-dependency</artifactId>
      <version>1.0-SNAPSHOT</version>
    </dependency>
  </dependencies>

  <build>
    <plugins>
      <plugin>
        <groupId>com.gitlab.smueller18.gitlab</groupId>
        <artifactId>gitlab-dependency-maven-plugin</artifactId>
        <version>1.0</version>
        <configuration>
          <groupIdRegex>^com\.gitlab\.smueller18.*$</groupIdRegex>
        </configuration>
      </plugin>
    </plugins>
  </build>

</project>
```

Finally, a `.gitlab-ci.yml` file must be placed in the project roots. Here is a minimalistic one:

```yaml
stages:
  - deploy
  - trigger

variables:
  MAVEN_CLI_OPTS: --batch-mode -s ci_settings.xml

deploy:
  stage: deploy
  image: maven:3.6.1-jdk-11
  script:
    - mvn $MAVEN_CLI_OPTS deploy

trigger:
  stage: trigger
  image: maven:3.6.1-jdk-11
  script:
    - mvn $MAVEN_CLI_OPTS com.gitlab.smueller18.gitlab:gitlab-dependency-maven-plugin:record
    - mvn $MAVEN_CLI_OPTS com.gitlab.smueller18.gitlab:gitlab-dependency-maven-plugin:trigger
```

## Development

### Release

-   Create new tag with `git tag -a x.x.x -m "tag x.x.x"`
-   Run `mvn deploy -Pgpg-sign`
